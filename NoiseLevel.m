function [x] = NoiseLevel( date )
%NoiseLevel search  the noise level analysed signal
%   After count occurring values of the 'dane', function looking
%   for most common numbers in matrix.
    [~, b] = find(dane == max(dane));
    n = b;
end
